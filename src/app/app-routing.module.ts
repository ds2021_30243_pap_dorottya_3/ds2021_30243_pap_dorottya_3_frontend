import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from "./core/guards/auth.guard";
import { APP_ROUTES } from "./core/routes/routes";
import { AdminComponent } from "./home/admin/admin.component";
import { ClientComponent } from "./home/client/client.component";
import { LoginComponent } from "./login/login.component";
import { AdminGuard } from "./core/guards/admin.guard";
import { ClientGuard } from "./core/guards/client.guard";
import { HomeComponent } from "./home/home.component";
import { MonitorComponent } from "./home/client/monitor/monitor.component";
import { ChartComponent } from "./home/client/chart/chart.component";

const routes: Routes = [
	{
		path: APP_ROUTES.HOME.path,
		component: HomeComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: APP_ROUTES.HOME.ADMIN.path,
				component: AdminComponent,
				canActivate: [AdminGuard]
			},
			{
				path: APP_ROUTES.HOME.CLIENT.path,
				component: ClientComponent,
				canActivate: [ClientGuard]
			},
			{
				path: APP_ROUTES.HOME.MONITOR.path,
				component: MonitorComponent,
				canActivate: [ClientGuard]
			},
			{
				path: APP_ROUTES.HOME.CHART.path,
				component: ChartComponent,
				canActivate: [ClientGuard]
			},
			{path: '**', redirectTo: APP_ROUTES.HOME.ADMIN.path}
		]
	},
	{path: APP_ROUTES.LOGIN.path, component: LoginComponent},

	// redirect to home in case of user entering anything else than predefined paths
	// ex: localhost:4201/asdsiys
	{path: '**', redirectTo: APP_ROUTES.HOME.path}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
