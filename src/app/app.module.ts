import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AdminComponent } from './home/admin/admin.component';
import { ClientComponent } from './home/client/client.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HeaderComponent } from './core/components/header/header.component';
import { HomeComponent } from './home/home.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DeviceComponent } from './home/admin/device/device.component';
import { SensorComponent } from './home/admin/device/sensor/sensor.component';
import { DialogBoxNewDeviceComponent } from './home/admin/dialog-box-new-device/dialog-box-new-device.component';
import { DialogBoxNewAccountComponent } from './home/admin/dialog-box-new-account/dialog-box-new-account.component';
import { MatDialogModule } from "@angular/material/dialog";
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { DialogBoxNewSensorComponent } from "./home/admin/device/dialog-box-new-sensor/dialog-box-new-sensor.component";
import { MatRadioModule } from "@angular/material/radio";
import { MonitorComponent } from "./home/client/monitor/monitor.component";
import { HistoryMonitorComponent } from './home/client/monitor/history-monitor/history-monitor.component';
import { ChartComponent } from './home/client/chart/chart.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { NgbdDatepickerPopupComponent } from './home/client/chart/ngbd-datepicker-popup/ngbd-datepicker-popup.component';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AppCategoryChartLineChartSingleSourceComponent } from './home/client/chart/app-category-chart-line-chart-single-source/app-category-chart-line-chart-single-source.component';
import { MatSnackBarModule } from "@angular/material/snack-bar";

@NgModule({
	declarations: [
		AppComponent,
		AdminComponent,
		ClientComponent,
		LoginComponent,
		HeaderComponent,
		HomeComponent,
		HeaderComponent,
		HomeComponent,
		DeviceComponent,
		SensorComponent,
		DialogBoxNewDeviceComponent,
		DialogBoxNewAccountComponent,
		DialogBoxNewSensorComponent,
  	MonitorComponent,
   HistoryMonitorComponent,
   ChartComponent,
   NgbdDatepickerPopupComponent,
   AppCategoryChartLineChartSingleSourceComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		ReactiveFormsModule,
		NoopAnimationsModule,
		MatFormFieldModule,
		MatFormFieldModule,
		FormsModule,
		MatDialogModule,
		MatButtonModule,
		MatInputModule,
		MatRadioModule,
		MatDatepickerModule,
		MatButtonModule,
		MatFormFieldModule,
		NgbModule,
		MatSnackBarModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}

