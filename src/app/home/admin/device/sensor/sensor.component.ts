import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SensorModel } from "../../../../core/models/sensor.model";
import { SensorService } from "../../../../core/services/sensor.service";
import { DeviceModel } from "../../../../core/models/device.model";
import { DeviceService } from "../../../../core/services/device.service";

@Component({
	selector: 'app-admin-device-sensor',
	templateUrl: './sensor.component.html',
	styleUrls: ['./sensor.component.scss']
})
export class SensorComponent implements OnInit {
	@Input() public device!: DeviceModel;
	@Input() public sensor!: SensorModel;
	@Output() public updated: EventEmitter<void> = new EventEmitter<void>();
	// public sensor: SensorModel | undefined;

	constructor(
		private sensorService: SensorService,
		private deviceService: DeviceService
	) { }

	async ngOnInit() {
		//await this.loadData(this.device);
	}

	async loadData(device: DeviceModel){
		// console.log(this.deviceService.getSensorByDevice(device.id));
		// this.sensor = await this.deviceService.getSensorByDevice(device.id);
	}

	async deleteSensor(sensor: SensorModel) {
		await this.sensorService.deleteSensor(sensor.id);

		this.updated.emit();
	}

	async updateSensor(sensor: SensorModel) {
		const updatedSensor = {...sensor};
		// updatedSensor.device = {...this.device};

		await this.sensorService.updateSensor(updatedSensor);

		this.updated.emit();
	}

}
