import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { SensorModel } from "../../../../core/models/sensor.model";

@Component({
	selector: 'app-dialog-box-new-sensor',
	templateUrl: './dialog-box-new-sensor.component.html',
	styleUrls: ['./dialog-box-new-sensor.component.scss']
})
export class DialogBoxNewSensorComponent {

	constructor(public dialogRef: MatDialogRef<DialogBoxNewSensorComponent>,
							@Inject(MAT_DIALOG_DATA) public data: SensorModel,
	) {}

	onNoClick(): void {
		this.dialogRef.close(this.data);
	}

}
