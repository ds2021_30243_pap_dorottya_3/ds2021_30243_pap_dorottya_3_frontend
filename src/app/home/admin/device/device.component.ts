import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DeviceService } from "../../../core/services/device.service";
import { DeviceModel } from "../../../core/models/device.model";
import { MatDialog } from "@angular/material/dialog";
import { DialogBoxNewSensorComponent } from "./dialog-box-new-sensor/dialog-box-new-sensor.component";
import { SensorModel } from "../../../core/models/sensor.model";
import { SensorService } from "../../../core/services/sensor.service";

@Component({
	selector: 'app-admin-device',
	templateUrl: './device.component.html',
	styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {
	@Input() public devices!: DeviceModel[];
	@Output() public updated: EventEmitter<void> = new EventEmitter<void>();

	constructor(
		private deviceService: DeviceService,
		private sensorService: SensorService,
		public dialog: MatDialog
	) { }

	ngOnInit() {
	}

	async deleteDevice(device: DeviceModel) {
		await this.deviceService.deleteDevice(device.id);

		this.updated.emit();
	}

	async updateDevice(device: DeviceModel) {
		await this.deviceService.updateDevice(device);

		this.updated.emit();
	}

	public addNewSensorForDevice(device: DeviceModel): void {
		const dialogRef = this.dialog.open(DialogBoxNewSensorComponent, {
			width: '450px',
			data: {},
		});

		dialogRef.afterClosed().subscribe(async (newSensor: SensorModel) => {
			//newSensor.device = device;

			await this.sensorService.insertDevicesSensor(newSensor, device.id);

			this.updated.emit();
		});
	}
}
