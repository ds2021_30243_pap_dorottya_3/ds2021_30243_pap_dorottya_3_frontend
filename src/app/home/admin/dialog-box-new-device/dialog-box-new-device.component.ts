import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { DeviceModel } from "../../../core/models/device.model";

@Component({
	selector: 'app-dialog-box-new-device',
	templateUrl: './dialog-box-new-device.component.html',
	styleUrls: ['./dialog-box-new-device.component.scss']
})
export class DialogBoxNewDeviceComponent {

	constructor(public dialogRef: MatDialogRef<DialogBoxNewDeviceComponent>,
							@Inject(MAT_DIALOG_DATA) public data: DeviceModel,
	) {}

	onNoClick(): void {
		this.dialogRef.close(this.data);
	}

}
