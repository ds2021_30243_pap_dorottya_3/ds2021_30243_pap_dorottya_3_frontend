import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { AccountModel } from "../../../core/models/account.model";

@Component({
	selector: 'app-dialog-box-new-account',
	templateUrl: './dialog-box-new-account.component.html',
	styleUrls: ['./dialog-box-new-account.component.scss']
})
export class DialogBoxNewAccountComponent {

	constructor(public dialogRef: MatDialogRef<DialogBoxNewAccountComponent>,
							@Inject(MAT_DIALOG_DATA) public data: AccountModel,
	) {}

	onNoClick(): void {
		this.dialogRef.close(this.data);
	}

}
