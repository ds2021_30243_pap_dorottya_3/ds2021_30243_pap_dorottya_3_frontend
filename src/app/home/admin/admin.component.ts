import { Component, OnInit } from '@angular/core';
import { AccountService } from "../../core/services/account.service";
import { AccountModel } from "../../core/models/account.model";
import { DeviceModel } from "../../core/models/device.model";
import { DeviceService } from "../../core/services/device.service";
import { MatDialog } from "@angular/material/dialog";
import { DialogBoxNewDeviceComponent } from "./dialog-box-new-device/dialog-box-new-device.component";
import { DialogBoxNewAccountComponent } from "./dialog-box-new-account/dialog-box-new-account.component";
import { SensorModel } from "../../core/models/sensor.model";
import { SensorService } from "../../core/services/sensor.service";
import { DialogBoxNewSensorComponent } from "./device/dialog-box-new-sensor/dialog-box-new-sensor.component";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
	public accounts: AccountModel[] = [];
	public availableDevices!: DeviceModel[];
	public availableSensors!: SensorModel[];
	public availableDevicesWithAccounts! : DeviceModel[];

  constructor(
		private accountService: AccountService,
		private deviceService: DeviceService,
		private sensorService: SensorService,
		public dialog: MatDialog
	) { }

  async ngOnInit() {
		await this.loadData();
	}

	public async loadData() {
		this.accounts = await this.accountService.getClientAccounts();
		this.availableDevices = await this.deviceService.getAvailableDevices();
		this.availableSensors = await this.sensorService.getAvailableSensors();
		this.availableDevicesWithAccounts = await this.deviceService.getAvailableDevicesWithAccounts();
	}

	async deleteAccount(account: AccountModel) {
		await this.accountService.deleteAccount(account.id);
		await this.loadData();
	}

	async updateAccount(account: AccountModel) {
		await this.accountService.updateAccount(account);
		await this.loadData();
	}

	public async insertDeviceToClient(accountId: any, deviceId: string) {
		console.log(accountId.value);
		await this.accountService.insertDeviceToClient(<string> accountId.value, deviceId);
		await this.loadData();
	}

	public async insertSensorToDevice(deviceId: any, sensorId: string) {
		await this.deviceService.insertSensorToDevice(<string> deviceId.value, sensorId);
		await this.loadData();
	}

	public addNewClient(): void {
		const dialogRef = this.dialog.open(DialogBoxNewAccountComponent, {
			width: '450px',
			data: {},
		});

		dialogRef.afterClosed().subscribe(async (newAccount: AccountModel) => {
			newAccount.type = "CLIENT";
			await this.accountService.insertAccount(newAccount);

			await this.loadData();
		});
	}

	public addNewDeviceForClient(account: AccountModel): void {
		const dialogRef = this.dialog.open(DialogBoxNewDeviceComponent, {
			width: '450px',
			data: {},
		});

		dialogRef.afterClosed().subscribe(async (newDevice: DeviceModel) => {
			newDevice.account = account;

			await this.deviceService.insertClientDevice(newDevice);

			await this.loadData();
		});
	}

	public addNewDevice(): void {
		const dialogRef = this.dialog.open(DialogBoxNewDeviceComponent, {
			width: '450px',
			data: {},
		});

		dialogRef.afterClosed().subscribe(async (newDevice: DeviceModel) => {

			await this.deviceService.insertDevice(newDevice);

			await this.loadData();
		});
	}

	public addNewSensor(): void {
		const dialogRef = this.dialog.open(DialogBoxNewSensorComponent, {
			width: '450px',
			data: {},
		});

		dialogRef.afterClosed().subscribe(async (newSensor: SensorModel) => {

			await this.sensorService.insertSensor(newSensor);

			await this.loadData();
		});
	}
}
