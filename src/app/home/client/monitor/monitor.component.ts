import { Component, OnInit } from '@angular/core';
import { AccountModel } from "../../../core/models/account.model";
import { AuthService } from "../../../core/services/auth.service";
import { MonitorValueService } from "../../../core/services/monitor-value.service";
import { MonitorValueModel } from "../../../core/models/monitor-value.model";
import { SensorModel } from "../../../core/models/sensor.model";
import { SensorService } from "../../../core/services/sensor.service";
import { AccountService } from "../../../core/services/account.service";

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.scss']
})
export class MonitorComponent implements OnInit {
	public account!: AccountModel;
	public currentValues: MonitorValueModel[] = [];
	public sensors: SensorModel[] = [];

  constructor(
		public authService: AuthService,
		public accountService: AccountService,
		public monitorValueService: MonitorValueService
	) { }

  async ngOnInit() {
		this.account = this.authService.getAccount();
		await this.loadData();
  }

	async loadData() {
		this.currentValues = await this.monitorValueService.insertCurrentMonitorValueForAccount(this.account.id);

		const latestAccountData = await this.accountService.getAccountById(this.account.id);
		this.sensors = latestAccountData.devices.map(d => d.sensor).filter(s => s && s.monitorValues && s.monitorValues.length);
	}



}
