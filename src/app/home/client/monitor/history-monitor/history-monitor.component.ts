import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SensorModel } from "../../../../core/models/sensor.model";

@Component({
  selector: 'app-history-monitor',
  templateUrl: './history-monitor.component.html',
  styleUrls: ['./history-monitor.component.scss']
})
export class HistoryMonitorComponent implements OnInit {
	@Input() public sensors: SensorModel[] = [];
	@Output() public updated: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
		// for (let device of this.account.devices) {
		// 	if(device.sensor) {
		// 		let sensor = device.sensor;
		// 		for (let monitorValue of sensor.monitorValues) {
		// 			console.log(monitorValue.value);
		// 		}
		// 	}
		// }
		// for (let sensor of this.sensors) {
		// 	for(let mov of sensor.monitorValues) {
		// 		console.log(mov);
		// 	}
		//
		// }
	}
}
