import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { MonitorValueService } from "../../../core/services/monitor-value.service";

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
	public model!: NgbDateStruct;

  constructor(
		public monitorValueService: MonitorValueService
	) { }

  ngOnInit(): void {
  }

	async load($event: NgbDateStruct) {
		this.model = await $event;
		console.log($event);
		//await this.generateChart();
	}

/*	async generateChart() {
		await this.monitorValueService.getMonitorValueByDate(this.model);
	}*/
}
