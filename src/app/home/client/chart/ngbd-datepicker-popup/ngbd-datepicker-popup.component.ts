import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { NgbDate } from "@ng-bootstrap/ng-bootstrap/datepicker/ngb-date";

@Component({
  selector: 'app-ngbd-datepicker-popup',
  templateUrl: './ngbd-datepicker-popup.component.html',
  styleUrls: ['./ngbd-datepicker-popup.component.scss']
})
export class NgbdDatepickerPopupComponent implements OnInit {
	model!: NgbDateStruct;
	date!: {year: number, month: number};

	@Output() public updated: EventEmitter<NgbDateStruct> = new EventEmitter<NgbDateStruct>();


	constructor(private calendar: NgbCalendar) { }

  ngOnInit(): void {
  }

	emitValue($event: NgbDateStruct) {
		console.log($event);
		this.updated.emit(this.model);
	}
}
