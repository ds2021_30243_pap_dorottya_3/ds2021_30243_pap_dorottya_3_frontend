import { Component, OnInit } from '@angular/core';
import { AccountModel } from "../../core/models/account.model";
import { DeviceModel } from "../../core/models/device.model";
import { AuthService } from "../../core/services/auth.service";

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
	public account!: AccountModel;
	public devices: DeviceModel[] = [];

  constructor(
		public authService: AuthService,
	) {
	}

	async ngOnInit() {
		this.account = this.authService.getAccount();
	}

}
