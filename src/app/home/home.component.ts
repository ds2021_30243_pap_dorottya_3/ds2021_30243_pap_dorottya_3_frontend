import { Component, OnInit } from '@angular/core';
import { AuthService } from "../core/services/auth.service";
import { ClientSocketService } from "../core/services/sockets/client-socket.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AccountModel } from "../core/models/account.model";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	public account!: AccountModel;

  constructor(
		public authService: AuthService,
		public clientSocketService: ClientSocketService,
		private snackBar: MatSnackBar,
	) { }

  ngOnInit(): void {
		this.account = this.authService.getAccount();

		this.subscribeToWebsocket();
		this.subscribeToNotifications();
  }

	private subscribeToWebsocket() {
		this.clientSocketService.subscribeToNotifications(this.account.id);
	}

	private subscribeToNotifications() {
		this.clientSocketService.subscribeToWebsocket((notification) => {
			console.log(1);
			let message = notification.body;
			console.log(notification.body);
			this.snackBar.open(message, 'Close', {
				duration: 30000
			})
		});
	}
}
