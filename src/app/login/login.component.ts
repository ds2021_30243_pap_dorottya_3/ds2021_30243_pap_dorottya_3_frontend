import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormControl, FormGroup} from '@angular/forms';
import {AuthService} from "../core/services/auth.service";
import {APP_ROUTES} from "../core/routes/routes";

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	public formGroup: FormGroup = new FormGroup({});

	public loginErrorMessage: string | undefined;

	constructor(
		private router: Router,
		private authService: AuthService
	) {
	}

	public get passwordForm() {
		return this.formGroup.controls.password;
	}

	public get usernameForm() {
		return this.formGroup.controls.username;
	}

	public ngOnInit() {
		this.loginErrorMessage = '';
		this.setupFormGroup();
	}

	public async login() {
		this.loginErrorMessage = '';
		try {
			let account = await this.authService.login(this.usernameForm.value, this.passwordForm.value);
			console.log(account);

			this.router.navigate([APP_ROUTES.HOME.url]);
		} catch (err: any) {
			if (err.status == 404) {
				this.loginErrorMessage = err.error.message;
			}
		}
	}

	private setupFormGroup() {
		this.formGroup = new FormGroup({
			username: new FormControl(''),
			password: new FormControl(''),
		});
	}
}
