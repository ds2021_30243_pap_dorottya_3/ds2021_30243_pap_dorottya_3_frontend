import {DeviceModel} from './device.model';

export interface AccountModel {
	id: string;
	name: string;
	address:string;
	age: number;
	accountName: string;
	password: string;
	type: string;
	devices: DeviceModel[];

	isOpen: boolean;
}
