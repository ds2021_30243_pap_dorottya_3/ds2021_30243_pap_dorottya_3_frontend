import {DeviceModel} from "./device.model";
import {MonitorValueModel} from "./monitor-value.model";

export interface SensorModel {
	id: string;
	description: string;
	maximumValue: number;
	// device: DeviceModel;
	monitorValues: MonitorValueModel[];
}
