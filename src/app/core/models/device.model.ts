import {AccountModel} from "./account.model";
import { SensorModel } from "./sensor.model";

export interface DeviceModel {
	id: string;
	description: string;
	location: string;
	maximumEnergyConsumption: number;
	averageEnergyConsumption: number;
	account: AccountModel;
	sensor: SensorModel;

	isOpen: boolean;
}
