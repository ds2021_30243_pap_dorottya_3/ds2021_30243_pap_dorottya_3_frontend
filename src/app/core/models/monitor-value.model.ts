import {SensorModel} from "./sensor.model";

export interface MonitorValueModel {
	id: string;
	timeStamp: string;
	value: number;
	sensor: SensorModel;
}
