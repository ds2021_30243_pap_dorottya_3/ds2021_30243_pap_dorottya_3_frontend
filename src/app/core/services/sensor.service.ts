import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SensorModel} from "../models/sensor.model";
import { DeviceUrl, SensorUrl } from "../routes/api.routes";
import { DeviceModel } from "../models/device.model";

@Injectable({providedIn: 'root'})
export class SensorService {
	constructor(private http: HttpClient) {
	}

	getSensors() {
		return this.http.get<SensorModel[]>(SensorUrl()).toPromise();
	}

	getAvailableSensors() {
		return this.http.get<SensorModel[]>(SensorUrl('/getAvailableSensors')).toPromise();
	}

	insertSensor(sensorModel: SensorModel) {
		return this.http.post<SensorModel>(SensorUrl(), sensorModel).toPromise();
	}

	insertDevicesSensor(sensorModel: SensorModel, deviceIdValue: String) {
		// console.log({sensorModel, deviceId});
		return this.http.post<SensorModel>(SensorUrl('/devicesSensor'), {sensor: sensorModel, deviceId: deviceIdValue}).toPromise();
	}

	getSensorById(sensorId: string) {
		return this.http.get<SensorModel>(SensorUrl('/' + sensorId)).toPromise();
	}

	updateSensor(sensorModel: SensorModel) {
		return this.http.put<SensorModel>(SensorUrl(), sensorModel).toPromise();
	}

	deleteSensor(sensorId: string) {
		return this.http.delete<void>(SensorUrl('/' + sensorId)).toPromise();
	}
}
