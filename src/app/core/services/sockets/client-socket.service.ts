import { Injectable } from "@angular/core";
import {Stomp} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';

@Injectable({ providedIn: 'root' })
export class ClientSocketService {
	private callbackFunctions: Array<{(notification: any): void; }> = [];

	constructor(
	) {
	}

	public subscribeToNotifications(clientId: string) {
		const URL = "https://backend-ass2.herokuapp.com/socket";

		const websocket = new SockJS(URL);

		const stompClient = Stomp.over(websocket);

		stompClient.connect({}, () => (
			stompClient.subscribe('/topic/socket/client/'+ clientId, notification => {
				this.callbackFunctions.forEach(s => s(notification))
			})
		))
	}

	public subscribeToWebsocket(callbackFunction: (notification: any) => void) {
		this.callbackFunctions.push(callbackFunction);
	}
}
