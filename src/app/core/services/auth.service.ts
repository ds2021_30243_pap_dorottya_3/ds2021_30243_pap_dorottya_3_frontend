import {Injectable} from "@angular/core";
import {HttpClient} from '@angular/common/http';
import {API_ROUTES, ApiUrl} from '../routes/api.routes';
import {AccountModel} from "../models/account.model";

@Injectable({providedIn: 'root'})
export class AuthService {

	constructor(private http: HttpClient) {
	}

	async login(username: string, password: string) {
		const response: any = await this.http.post(ApiUrl(API_ROUTES.LOGIN), { accountName: username, password: password }).toPromise();
		console.log(response);

		const account: AccountModel = response;
		if (account !== undefined) {
			localStorage.setItem('account', JSON.stringify(account));

			return account;
		}
		throw response.message;
	}

	logout() {
		localStorage.removeItem('account');
	}

	public getAccount() {
		return <AccountModel>JSON.parse(<string>localStorage.getItem('account'));
	}
}
