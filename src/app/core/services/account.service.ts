import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {AccountModel} from "../models/account.model";
import {AccountUrl} from "../routes/api.routes";

@Injectable({providedIn: 'root'})
export class AccountService {

	constructor(private http: HttpClient) {
	}

	getAccounts() {
		return this.http.get<AccountModel[]>(AccountUrl()).toPromise();
	}

	getClientAccounts() {
		return this.http.get<AccountModel[]>(AccountUrl('/allClients/')).toPromise();
	}

	insertDeviceToClient(accountId: string, deviceId: string) {
		console.log({accountId, deviceId});
		return this.http.post<AccountModel>(AccountUrl('/addDeviceToClient'), {accountId: accountId, deviceId: deviceId}).toPromise();
	}

	insertAccount(accountModel: AccountModel) {
		return this.http.post<AccountModel>(AccountUrl(), accountModel).toPromise();
	}

	getAccountById(accountId: string) {
		return this.http.get<AccountModel>(AccountUrl('/' + accountId)).toPromise();
	}

	updateAccount(accountModel: AccountModel) {
		return this.http.put<AccountModel>(AccountUrl(), accountModel).toPromise();
	}

	deleteAccount(accountId: string) {
		return this.http.delete<void>(AccountUrl('/' + accountId)).toPromise();
	}
}
