import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {MonitorValueModel} from "../models/monitor-value.model";
import {MonitorValueUrl} from "../routes/api.routes";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

@Injectable({providedIn: 'root'})
export class MonitorValueService {
	constructor(private http: HttpClient) {
	}

	getMonitorValues() {
		return this.http.get<MonitorValueModel[]>(MonitorValueUrl()).toPromise();
	}

	insertMonitorValue(monitorValueModel: MonitorValueModel) {
		return this.http.post<MonitorValueModel>(MonitorValueUrl(), monitorValueModel).toPromise();
	}

	insertCurrentMonitorValueForAccount(accountId: string) {
		return this.http.post<MonitorValueModel[]>(MonitorValueUrl('/insertCurrentMonitorValueForAccount/' + accountId), {}).toPromise();
	}

	getMonitorValueById(monitorValueId: string) {
		return this.http.get<MonitorValueModel>(MonitorValueUrl('/' + monitorValueId)).toPromise();
	}

	/*getMonitorValueByDate(date: NgbDateStruct) {
		return this.http.get<MonitorValueModel[]>(MonitorValueUrl('/getMonitorValueByDate/' + date)).toPromise();
	}*/

	updateMonitorValue(monitorValueModel: MonitorValueModel) {
		return this.http.put<MonitorValueModel>(MonitorValueUrl(), monitorValueModel).toPromise();
	}

	deleteMonitorValue(monitorValueId: string) {
		return this.http.delete<void>(MonitorValueUrl('/' + monitorValueId)).toPromise();
	}
}
