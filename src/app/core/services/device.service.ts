import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import { DeviceUrl } from "../routes/api.routes";
import {DeviceModel} from "../models/device.model";

@Injectable({providedIn: 'root'})
export class DeviceService {
	constructor(private http: HttpClient) {
	}


	getDevices() {
		return this.http.get<DeviceModel[]>(DeviceUrl()).toPromise();
	}

	getAvailableDevices() {
		return this.http.get<DeviceModel[]>(DeviceUrl('/getAvailableDevices')).toPromise();
	}

	getAvailableDevicesWithAccounts() {
		return this.http.get<DeviceModel[]>(DeviceUrl('/getAvailableDevicesWithAccount')).toPromise();
	}

	insertDevice(deviceModel: DeviceModel) {
		return this.http.post<DeviceModel>(DeviceUrl(), deviceModel).toPromise();
	}

	async insertClientDevice(deviceModel: DeviceModel) {
		return this.http.post<DeviceModel>(DeviceUrl('/clientDevice'), deviceModel).toPromise();
	}

	async insertSensorToDevice(deviceId: string, sensorId: string) {
		console.log({deviceId, sensorId});
		return this.http.post<DeviceModel>(DeviceUrl('/addSensorToDevice'), {deviceId: deviceId, sensorId: sensorId}).toPromise();
	}

	getDeviceById(deviceId: string) {
		return this.http.get<DeviceModel>(DeviceUrl('/' + deviceId)).toPromise();
	}

	/*getSensorByDevice(deviceId: string) {
		return this.http.get<SensorModel>(DeviceUrl('/getSensorByDevice/' + deviceId)).toPromise();
	}*/

	updateDevice(deviceModel: DeviceModel) {
		return this.http.put<DeviceModel>(DeviceUrl(), deviceModel).toPromise();
	}

	deleteDevice(deviceId: string) {
		return this.http.delete<void>(DeviceUrl('/' + deviceId)).toPromise();
	}
}
