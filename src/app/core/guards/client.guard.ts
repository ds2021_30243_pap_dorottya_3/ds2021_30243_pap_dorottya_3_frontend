import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {APP_ROUTES} from "../routes/routes";

@Injectable({ providedIn: 'root' })
export class  ClientGuard implements CanActivate {
	constructor(
		private router: Router,
		private authService: AuthService
	) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		console.log('ClientGuard');
		if (this.authService.getAccount().type === "CLIENT") {
			// is admin so return true
			return true;
		}

		// not admin in so redirect to login page
		this.router.navigate([APP_ROUTES.HOME.ADMIN.url()]);
		return false;
	}
}
