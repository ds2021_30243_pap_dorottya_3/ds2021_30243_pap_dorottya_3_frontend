import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {APP_ROUTES} from "../routes/routes";

@Injectable({ providedIn: 'root' })
export class  AdminGuard implements CanActivate {
	constructor(
		private router: Router,
		private authService: AuthService
	) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

		if (this.authService.getAccount().type === "ADMIN") {
			// is admin so return true
			return true;
			console.log('admin');
		}

		console.log('adminguard');
		// not admin in so redirect to login page
		this.router.navigate([APP_ROUTES.HOME.CLIENT.url()]);
		return false;
	}
}
