import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {APP_ROUTES} from "../routes/routes";
import {Injectable} from "@angular/core";

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
	constructor(
		private router: Router,
		private authService: AuthService
	) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		const currentUser = this.authService.getAccount();
		console.log('authguard')

		// login only if a user is stored in the local storage
		if (currentUser) {
			// authorised so return true
			return true;
		}

		// not logged in so redirect to login page
		this.router.navigate([APP_ROUTES.LOGIN.url]);
		return false;
	}
}
