export const API_ROUTES = {
	ALL: '/',
	LOGIN: '/auth/login',
	DEVICE: '/device',
	ACCOUNT: '/account',
	MONITOR_VALUE: '/monitorValue',
	SENSOR: '/sensor'
}

export function ApiUrl(path: any) {
	return `https://backend-ass2.herokuapp.com${path}`;
}

export function AccountUrl(path?: string) {
	return `${ApiUrl(API_ROUTES.ACCOUNT)}${path ? path : ''}`;
}

export function DeviceUrl(path?: string) {
	return `${ApiUrl(API_ROUTES.DEVICE)}${path ? path : ''}`;
}

export function MonitorValueUrl(path?: string) {
	return `${ApiUrl(API_ROUTES.MONITOR_VALUE)}${path ? path : ''}`;
}

export function SensorUrl(path?: string) {
	return `${ApiUrl(API_ROUTES.SENSOR)}${path ? path : ''}`;
}
