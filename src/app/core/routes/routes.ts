export const APP_ROUTES = {
	LOGIN: {
		url: '/login',
		path: 'login'
	},
	HOME: {
		url: '/home',
		path: 'home',
		CLIENT: {
			url: getClientUrl,
			path: 'client'
		},
		MONITOR: {
			url: getMonitorUrl,
			path: 'monitor'
		},
		CHART: {
			url: getChartUrl,
			path: 'chart'
		},
		ADMIN: {
			url: getAdminUrl,
			path: 'admin'
		}
	},
}

function getClientUrl(): string {
	return `${APP_ROUTES.HOME.url}/${APP_ROUTES.HOME.CLIENT.path}`;
}

function getMonitorUrl(): string {
	return `${APP_ROUTES.HOME.url}/${APP_ROUTES.HOME.MONITOR.path}`;
}

function getChartUrl(): string {
	return `${APP_ROUTES.HOME.url}/${APP_ROUTES.HOME.CHART.path}`;
}

function getAdminUrl(): string {
	return `${APP_ROUTES.HOME.url}/${APP_ROUTES.HOME.ADMIN.path}`;
}
