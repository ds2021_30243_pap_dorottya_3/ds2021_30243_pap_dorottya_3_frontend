import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { APP_ROUTES } from "../../routes/routes";
import { Router } from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

	logout() {
		this.authService.logout();
		this.router.navigate([APP_ROUTES.LOGIN.url]);
	}

	public get isClient() {
		return this.authService.getAccount()?.type === "CLIENT";
	}

	public async goToHome() {
			this.router.navigate([APP_ROUTES.HOME.url]);
	}

	public async goToMonitor() {
			this.router.navigate([APP_ROUTES.HOME.MONITOR.url()]);
	}

	public async goToChart() {
		this.router.navigate([APP_ROUTES.HOME.CHART.url()]);
	}
}
